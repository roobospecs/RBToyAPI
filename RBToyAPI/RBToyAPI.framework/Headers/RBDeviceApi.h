//
//  RBDeviceApi.h
//  Pods
//
//  Created by baxiang on 16/11/12.
//
//

#import <Foundation/Foundation.h>
#import "RBDeviceModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface RBDeviceApi : NSObject

#pragma mark ------------------- 声波配网 ------------------------
/**
 生成配网声波
 @param wifiName wifi 名称
 @param wifiPwd wifi 密码
 @param block 声波url 文件
 */
+(void)getSoundWave:(NSString *)wifiName wifiPsd:(NSString *)wifiPwd block:(nonnull void (^)(NSString* urlStr,NSError*_Nullable error)) block;

/**
 获取声波配网的结果  建议采用定时轮询
*/
+(void)getDeviceBindInfo:(void (^)(RBBindInfo* _Nullable response, NSError *_Nullable error))block;

#pragma mark ------------------- 设备聊天 ------------------------

/**
（旧接口）
 发送文本内容
 @param text 文字内容
 */
+ (void)sendTTSText:(NSString *)text block:(void (^)(id response,NSError *error))block;

/**
（旧接口）
 发送声音文件
 @param filePath 文件url 路径
 @param progressBlock 上传进度
 @param resultBlock 上传结果
 */
+ (void)sendVoice:(NSString *)filePath length:(NSUInteger)length progressBlock:(void(^)(NSProgress *progress))progressBlock  resultBlock:(void (^)(id  response, NSError * _Nullable))resultBlock;

/**
（旧接口）
 获取聊天记录的列表
 @param fromID 首次为0 其他使用当前聊天记录列表最后的RBChatModel的chatID
 */
+(void)getChatMessageList:(NSNumber*)fromID block:(void (^)(RBChatList *chatList,NSError *error))block;

/**
 设置用户推送的 token
 @param pushToken apple pushToken
 */
+ (void)setPushToken:(NSString *_Nonnull)pushToken block:(nullable void (^)(BOOL  isSuccess,NSError *error)) block;

/**
 设置音频文件已读取
 @param chatID 消息id
 */
+(void)readChatReport:(NSString*)chatID block:(nullable void (^)(BOOL  isSuccess,NSError *error)) block;

/**
 (新接口)
 发送群组文本内容
 @param text 文字内容
 */
+ (void)sendChatGroupTTSText:(NSString *)text block:(void (^)(RBChatGroupModel *chatModel,NSError *error))block;

/**
（新接口）
 发送群组声音文件
 @param filePath 文件url 路径
 @param progressBlock 上传进度
 @param resultBlock 上传结果
 */
+ (void)sendChatGroupVoice:(NSString *)filePath length:(NSUInteger)length progressBlock:(void(^)(NSProgress *progress))progressBlock  resultBlock:(void (^)(RBChatGroupModel *chatModel, NSError * _Nullable))resultBlock;

/**
 （新接口）
 获取群组聊天记录列表
 SDK使用者需要记录最新的chatId,和当前返回的最老的chatId，以便查询历史聊天记录
 @param chatID 聊天消息ID 首次为nil，默认取最新的
 @param isLatest 是否为最新(YES->返回该chatID之后的消息 NO->返回该chatID之前的消息)首次传NO，默认取最新的
 如果chatModel.body.length=0时，根据音频的采样率，采样位数
 length计算：chatModel.body.size * 1.0f / (16000 * (16 / 8) * 1)
 */
+(void)getChatGroupList:(NSString*)chatID isLatest:(BOOL)isLatest block:(void (^)(RBGroupChatList *chatList,NSError *error))block;

/**
 清空聊天全部聊天消息
 @param chatID 删除chatID之前的所有消息
 */
+(void)clearChatList:(NSString*)chatID block:(void (^)(BOOL isSuccess,NSError *error))block;

#pragma mark ------------------- 设备信息 ------------------------

/**
 获取用户的所有设备

 @param currDetail 是否显示当前控制设备的详情  YES等于同时调用getDeviceDetail
 */
+ (void)getDeviceList:(BOOL)currDetail block:(void(^)(NSArray <RBDeviceModel*>*device,NSError* _Nullable error))block;

/**
 获取设备详细信息
 */
+ (void)getDeviceDetail:(nonnull void (^)(RBDevicesDetail *detail,NSError* _Nullable error))block;

/**
  获取设备硬件信息
 */
+ (void)getHardwareInfo:(nonnull void(^)(RBHardwareModel *hardMode,NSError* _Nullable error))block;


#pragma mark ------------------- 设备控制 ------------------------
/**
 功能1：--------------------------- 自定义消息控制设备耳灯童锁
 发送自定义控制命令(如耳灯 童锁)
 @param params 请求参数
 */
+ (void)sendCustomCommand:(NSDictionary*)params block:(void (^)(BOOL isSuccess,NSError *error))block;

#pragma mark ------------------- 自定义消息实现APP运动控制和运动编程
/**
 功能1：--------------------------- 自定义消息实现APP运动控制
 运动控制命令：1向前走(S92T)，2向后退(SB2T)，3向左转(S52T)，4向右转(S72T)，5跳个舞(SJ0T)，6停止(SO0T)
 @param params 请求参数params = @{@"RobotControl":@"运动控制命令"}
 功能2：--------------------------- 自定义消息实现运动编程
 运动编程：1向前走(S92T)，2向后退(SB2T)，3向左转(S52T)，4向右转(S72T)
 @param params 请求参数params = @{@"RobotControl":@"运动控制命令拼接的字符串，最多动作组合为9个"}
 运动停止（停止执行的编程程序）：停止(SO0T)
 @param params
 @param block
 */
+ (void)sendRobotControl:(NSDictionary*)params block:(void (^)(BOOL isSuccess,NSError *error))block;
/**
 重启设备
 @param block isSucceed 是否成功 error 错误信息
 */
+ (void)restartDevice:(void (^)(BOOL isSuccess,NSError *error))block;

/**
 关闭设备
 @param block isSucceed 是否成功 error 错误信息
 */
+ (void)shutdownDevice:(void (^)(BOOL isSuccess,NSError* _Nullable error))block;

/**
 修改设备的名称
 @param deviceName 新的名称
 */
+ (void)updateDeviceName:(NSString *_Nonnull)deviceName block:(void (^)(BOOL isSuccess,NSError* _Nullable error)) block;

/**
 修改设备音量
 @param voiceValue 音量值 最小值0 最大值100

 */
+ (void)changeDeviceVolume:(NSInteger )voiceValue block:(void (^)(BOOL isSuccess,NSError* _Nullable error)) block;

/**
 删除当前的设备
 */
+ (void)deleteDevice:(void (^)(BOOL isSuccess,NSError *_Nullable error)) block;
#pragma mark -------------------版本升级------------------------

/**
    检测是否有新版本
 */
+(void)checkDeviceVersion:(void (^)(BOOL update,NSString *version,NSError * _Nullable error))block;

/**
    设备升级
 */
+(void)updateDevice:(void (^)(BOOL isSuccess,NSError *error))block;

/**
 获取消息列表
 @param startid 起始ID 首次请求传0 下次设置startid==response数组的lastObject的RBMessageModel的mID
*/
+(void)getMessageList:(NSString*)startid block:(void (^)(NSArray<RBMessageModel*>*response,NSError *error)) block;

/**
 清除消息
 @param mIDArray 消息的id
 */
+(void)deleteMessage:(NSArray*)mIDArray block:(void (^)(BOOL isSuccess,NSError *error))block;

/**
 获取闹钟列表
 @param typeArray 闹钟类型（NSInteger）：1(自定义)，2(起床)，3(吃饭)，4(喝水)，5(休息),获取列表是传入需要获取的闹钟列表类型
 */
+(void)getAlarmList:(NSArray*)typeArray block:(void (^)(RBAlarmList *alarmList, NSError *error))block;

/**
 增加闹钟
 */
+ (void)addAlarm:(RBAlarmModel*)model block:(void (^)(RBAlarmModel *model,NSError *error)) block;

/**
 修改闹钟
 */
+ (void)editAlarm:(RBAlarmModel*)model block:(void (^)(BOOL isSuccess,NSError *error)) block;

/**
 删除闹钟
 @param alarmIds 闹钟ID数组
 */
+ (void)deleteAlarm:(NSArray*)alarmIds block:(void (^)(BOOL isSuccess,NSError *error)) block;

/**
 定时关机
 @param timer 时间
 */
+(void)timedShutdown:(NSInteger)timer status:(BOOL)status block:(void (^)(RBAlarmModel *model,NSError *error))block;
#pragma mark ------------------- 视频鉴权 ------------------------

/**
 获取设备的视频云UID
 @param resultBlock response视频云信息
 */
+ (void)getDeviceVideoUID:(void (^)(RBVideoModel *response, NSError * _Nullable error))resultBlock;

@end
NS_ASSUME_NONNULL_END
