Pod::Spec.new do |s|
  s.name = "RBToyAPI"
  s.version="1.5.7"
  s.summary = "ROOBO机器人_iOS_SDK"
  #s.license = {"type"=>"BSD", "file"=>"LICENSE"}
  s.authors = {"chenwu"=>"chenwu@roobo.com"}
  s.homepage = "https://gitee.com/roobospecs/RBToyAPI"
  s.description = "ROOBO机器人sdk"
  s.frameworks = ["UIKit", "SystemConfiguration", "MobileCoreServices", "AVFoundation"]
  s.requires_arc = true
  s.xcconfig = {"OTHER_LDFLAGS"=>"-ObjC"}
  s.source  = { :git => 'git@gitee.com:roobospecs/RBToyAPI.git', :tag => s.version.to_s }
  s.ios.deployment_target    = '7.0'
  s.ios.vendored_framework   = 'RBToyAPI/RBToyAPI.framework'
end
